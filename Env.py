import numpy as np
from obstacles import mountain_coordinates, water_coordinates, forest_coordinates

class Env():
    def __init__(self):
        self.height = 6;
        self.width = 10;
        self.posX = 0;
        self.posY = self.height-1;
        self.endX = self.width-1;
        self.endY = 0;
        self.actions = [0, 1, 2, 3];
        self.stateCount = self.height*self.width;
        self.actionCount = len(self.actions);
        self.rewardXValue = 1.75
        self.rewardYValue = 0.5
        self.rewardPositionValue = 0.035
        self.stateDiminisher = 0.05
        self.mountainRewardDiminisher = 0.05
        self.forestRewardDiminisher = 0.025
        self.waterRewardDiminisher = 1
        self.visitedStates = []


    def reset(self):
        self.posX = 0;
        self.posY = self.height-1;
        self.done = False;
        self.visitedStates = []
        return 0, 0, False;

    # take action
    def step(self, action):
        # Mapeamos el estado actual (x,y)
        actualState = self.width * self.posY + self.posX;

        # Almacenamos todos los estados visitados
        self.visitedStates.append(actualState);

        if action==0: # left
            self.posX = self.posX-1 if self.posX>0 else self.posX;
        if action==1: # right
            self.posX = self.posX+1 if self.posX<self.width-1 else self.posX;
        if action==2: # up
            self.posY = self.posY-1 if self.posY>0 else self.posY;
        if action==3: # down
            self.posY = self.posY+1 if self.posY<self.height-1 else self.posY;

        # Marcamos como hecho si hemos alcanzado la posicion final
        done = self.posX == self.endX and self.posY == self.endY;
        
        # Mapeamos el siguiente estado (x,y)
        nextState = self.width * self.posY + self.posX;

        # Comprobamos el numero de ocurrencias del mismo estado para restarle valor (evitamos bucles)
        diminisher = self.visitedStates.count(nextState)

        # para calcular la recompensa comprobaremos el tipo de zona al que ha llegado
        reward, zonesReward = self.calculateReward(self.posX, self.posY, done, diminisher);
        return nextState, reward, done, zonesReward;

    # return a random action
    def randomAction(self):
        return np.random.choice(self.actions);

    # display environment
    def render(self):
        for i in range(self.height):
            for j in range(self.width):
                # my position
                if self.posY==i and self.posX==j:
                    print("O", end='');
                # objective position
                elif self.endY==i and self.endX==j:
                    print("B", end='');
                # water
                elif (j, i) in water_coordinates:
                    print("W", end='');
                # forest
                elif (j, i) in forest_coordinates:
                    print("F", end='')
                # mountain
                elif (j, i) in mountain_coordinates:
                    print("M", end='')
                else:
                    print(".", end='');
            print("");

    # calculate reward 
    '''
    Los siguientes pasos deberian ser: Indicar diferentes metricas:
        - Un reward que es el que existe actualmente para ayudar a que el agente encuentre la salida rapido
        - Un reward a devolver (que solo tiene en cuenta las zonas pisadas)
        - El numero de tipos de zona que ha pisado por el camino
    '''
    def calculateReward(self, posX, posY, done, stateDiminisher):
        position = (posX, posY);

        # El parametro stateDiminisher de la clase nos ayuda a equilibrar el coste por visitar repetidamente el mismo estado
        stateDiminisherParam = self.stateDiminisher * stateDiminisher

        '''
        Definimos una recompensa basada en la posicion (mayor cuanto mas cerca del final) y en la cantidad de veces que un estado ha sido visitado
        acotada por 4 parametros que consiguen unos valores consistentes con los del resto de la tabla de valores Q

            - rewardXValue: Maximiza el valor de la componente X para ayudar al agente a cruzar la parte central del tablero
            - rewardYValue: Disminuye el valor de la componente Y para no provocar que el agente suba sin mas porque la recompensa es mayor
            - rewardPositionValue: Parametro global que calibra el valor que le asignamos a la posicion (para escalarla respecto a otras recompensas)
            - stateDiminisherParam: Penaliza en funcion de cuantas veces se ha visitado ya un estado (evitamos bucles)
        '''
        tuning = ((posX * self.rewardXValue + (9 - posY) * self.rewardYValue) * self.rewardPositionValue) - stateDiminisherParam
        
        if done:
            return tuning + 1, 1;
        elif position in forest_coordinates:
            return tuning - self.forestRewardDiminisher, -self.forestRewardDiminisher;
        elif position in mountain_coordinates:
            return tuning - self.mountainRewardDiminisher, -self.mountainRewardDiminisher;
        elif position in water_coordinates:
            return tuning - self.waterRewardDiminisher, -self.waterRewardDiminisher;
        else:
            return tuning + 0, 0;
