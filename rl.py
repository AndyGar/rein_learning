from Env import Env
import numpy as np
import time
import os

'''
Basicamente lo que esta pasando es que tiene demasiada importancia la recompensa del siguiente estado, 
por tanto intenta siempre volver a la llanura en lugar de buscar el punto final
en lugar de usar una politica greedy, deberiamos tratar de incorporar un tipo de politica
que tienda a llegar al punto final. En lugar de depender unicamente de la recompensa (value)
'''
# create environment
env = Env()

# QTable : contains the Q-Values for every (state,action) pair
# Contiene 25(stateCount) arrays de 4(actionCount) números entre el 0 y el 1
qtable = np.random.rand(env.stateCount, env.actionCount).tolist()

# Almacenamos la recompensa mas alta conseguida
highestReward = 0

# hyperparameters
epochs = 50
gamma = 0.15
epsilon = 0.15
decay = 0.5

# training loop
for i in range(epochs):
    state, reward, done = env.reset()
    steps = 0
    sumReward = 0

    while not done:
        os.system('clear')
        print("epoch #", i+1, "/", epochs)
        env.render()
        time.sleep(0.05)

        # count steps to finish game
        steps += 1

        # act randomly sometimes to allow exploration
        if np.random.uniform() < epsilon:
            action = env.randomAction()

        # if not select max action in Qtable (act greedy)
        else:
            action = qtable[state].index(max(qtable[state]))

        # take action
        next_state, reward, done, zonesReward = env.step(action)

        # update qtable value with Bellman equation
        qtable[state][action] = reward + gamma * max(qtable[next_state])

        # update the reward
        sumReward += zonesReward
        print(sumReward)

        # update state
        state = next_state

    # The more we learn, the less we take random actions
    epsilon -= decay * epsilon
    
    # update the highest reward
    if sumReward > highestReward:
        print("\nNew highest reward!")
        highestReward = sumReward

    print("\nDone in", steps, "steps".format(steps))
    print("\nReward: ", sumReward)
    time.sleep(1.5)

print("\n HIGHEST REWARD: ", str(round(highestReward, 4)))

